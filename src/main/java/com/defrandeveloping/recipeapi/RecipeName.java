package com.defrandeveloping.recipeapi;

/**
 * Created by anthonydefrancesco on 2/7/15.
 */
public class RecipeName {
    public String recipeName;

    public RecipeName() {
    }

    public String getRecipeName() {
        return recipeName;
    }

    public void setRecipeName(String recipeName) {
        this.recipeName = recipeName;
    }
}
