package com.defrandeveloping.recipeapi;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.users.User;

import java.util.Date;

/**
 * Created by anthonydefrancesco on 2/6/15.
 */
public class Recipe {

    public String recipeName;
    public String category;
    public String tags;
    public Key recipeKey;
    public Date createdDate;
    public Date lastModifiedDate;
    public User owner;

    public Recipe() {

    }

    public String getRecipeName() {
        return recipeName;
    }

    public void setRecipeName(String recipeName) {
        this.recipeName = recipeName;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public Key getRecipeKey() {
        return recipeKey;
    }

    public void setRecipeKey(Key recipeKey) {
        this.recipeKey = recipeKey;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }
}
