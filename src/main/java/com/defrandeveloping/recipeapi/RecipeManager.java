package com.defrandeveloping.recipeapi;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.Named;
import com.google.appengine.api.datastore.*;
import com.google.appengine.api.oauth.OAuthRequestException;
import com.google.appengine.api.users.User;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Api(
        name = "recipe",
        version = "beta",
        scopes = {Constants.EMAIL_SCOPE},
        clientIds = {Constants.WEB_CLIENT_ID, Constants.ANDROID_CLIENT_ID, Constants.API_EXPLORER_CLIENT_ID, Constants.IOS_CLIENT_ID},
        audiences = {Constants.ANDROID_AUDIENCE}
)
public class RecipeManager {
    //todo add ingredient
    //todo add step
    //todo add additional items for social features #4
    //todo add fork/copy api references #6
    //todo add follower system - recipes - implement people later
    //todo delete recipe and ingredient/steps
    //todo add user settings

    DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

    @ApiMethod(name = "updateRecipe", httpMethod = "put")
    public Entity updateRecipe(User user, Recipe recipe) throws OAuthRequestException, IOException, EntityNotFoundException {
        Key recipeKey = KeyFactory.createKey("Recipe", recipe.getRecipeKey().getId());
        Entity recipeEntity = datastore.get(recipeKey);
        //TODO check if null/different and only update the different ones
        recipeEntity.setProperty("recipeName", recipe.getRecipeName());
        recipeEntity.setProperty("category", recipe.getCategory());
        recipeEntity.setProperty("createdDate", new Date());
        recipeEntity.setProperty("modifiedDate", new Date());
        recipeEntity.setProperty("owner", user);
        recipeEntity.setProperty("tags", recipe.getTags());
        datastore.put(recipeEntity);
        return recipeEntity;
    }

    @ApiMethod(name = "getAllRecipes", httpMethod = "get")
    public ArrayList<Recipe> listRecipes(User user) throws OAuthRequestException, IOException {
        ArrayList<Recipe> recipes = new ArrayList<>();
        Query.Filter filter = new Query.FilterPredicate("owner", Query.FilterOperator.EQUAL, user);
        Query q = new Query("Recipe").setFilter(filter).addSort("recipeName", Query.SortDirection.ASCENDING);
        List<Entity> recipeEntityList = datastore.prepare(q).asList(FetchOptions.Builder.withDefaults());
        for (Entity recipeEntity : recipeEntityList) {
            Recipe recipe = new Recipe();
            recipe.setRecipeKey(recipeEntity.getKey());
            try {
                recipe.setCategory(recipeEntity.getProperty("category").toString());
            } catch (Exception ignored) {

            }
            try {
                recipe.setTags(recipeEntity.getProperty("tags").toString());
            } catch (Exception ignored) {
            }
            try {
                recipe.setRecipeName(recipeEntity.getProperty("recipeName").toString());
            } catch (Exception ignored) {
            }
            try {
                recipe.setOwner((User) recipeEntity.getProperty("owner"));
            } catch (Exception ignored) {

            }
            try {
                recipe.setCreatedDate((Date) recipeEntity.getProperty("createdDate"));
            } catch (Exception ignored) {

            }
            try {
                recipe.setLastModifiedDate((Date) recipeEntity.getProperty("modifiedDate"));
            } catch (Exception ignored) {

            }
            recipes.add(recipe);
        }
        return recipes;
    }

    @ApiMethod(name = "add", httpMethod = "post")
    public Recipe insertRecipe(User user, Recipe recipe) throws OAuthRequestException, IOException {
        recipe.setOwner(user);
        Entity recipeEntity = new Entity("Recipe");
        recipeEntity.setProperty("recipeName", recipe.getRecipeName());
        recipeEntity.setProperty("category", recipe.getCategory());
        recipeEntity.setProperty("createdDate", new Date());
        recipeEntity.setProperty("modifiedDate", new Date());
        recipeEntity.setProperty("owner", recipe.getOwner());
        recipeEntity.setProperty("tags", recipe.getTags());
        datastore.put(recipeEntity);
        recipe.setRecipeKey(recipeEntity.getKey());
        return recipe;
    }

    @ApiMethod(name = "getRecipe", httpMethod = "get")
    public Entity getRecipe(User user, @Named("id") long id) throws OAuthRequestException, IOException, EntityNotFoundException {
        Key recipeKey = KeyFactory.createKey("Recipe", id);
        return datastore.get(recipeKey);
    }

    @ApiMethod(name = "getRecipeNames", httpMethod = "get")
    public ArrayList<RecipeName> getRecipeNames(User user) throws OAuthRequestException, IOException {
        ArrayList<RecipeName> recipeNames = new ArrayList<>();
        Query.Filter filter = new Query.FilterPredicate("owner", Query.FilterOperator.EQUAL, user);
        Query q = new Query("Recipe").setFilter(filter);
        List<Entity> recipeEntityList = datastore.prepare(q).asList(FetchOptions.Builder.withDefaults());
        for (Entity recipeEntity : recipeEntityList) {
            RecipeName recipeName = new RecipeName();
            recipeName.setRecipeName(recipeEntity.getProperty("recipeName").toString());
            recipeNames.add(recipeName);
        }
        return recipeNames;
    }
}