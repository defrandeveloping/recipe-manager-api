var app = angular.module('RecipeManager', ['ngMaterial', 'ngRoute', 'angular-google-gapi'])
    .config(function ($mdThemingProvider) {
        $mdThemingProvider.theme('default')
            .primaryPalette('light-blue')
            .accentPalette('light-green');
    });
app.run(['GAuth', 'GApi', '$location', '$rootScope',
    function (GAuth, GApi, $location, $rootScope) {
        var CLIENT = '496523996285-troa4352i42unp87ilqc88kftu0ie2nj.apps.googleusercontent.com';
        var BASE = 'https://natural-aria-848.appspot.com/_ah/api';
        GApi.load('recipe', 'beta', BASE);
        GAuth.setClient(CLIENT);
        GAuth.checkAuth().then(
            function () {
                $location.path('/');
                $rootScope.refresh = true;
            },
            function () {
                //todo add login page
                //$location.path('/'); // an example of action if it's possible to
                // authenticate user at startup of the application
            });
        $rootScope.loginButton = function () {
            GAuth.login().then(function () {
                $rootScope.refresh = true;
                $location.path('/');
            }, function () {
                //todo 
                console.log('login fail');
            });
        };
    }]);

app.controller('LogController', ['$scope', '$log',
    function ($scope, $log) {
        $scope.$log = $log;
        $scope.message = 'Hello World!';
    }]);

app.directive('logindirective',
    function () {
        return {
            restrict: 'E',
            replace: true,
            scope: {},
            template: '<span>{{$root.user.name}}</span>',
            controller: function ($rootScope) {
            }
        }
    });

app.controller("RecipeController", ['$scope', '$mdSidenav', '$location', 'recipeService', 'GApi',
    function ($scope, $mdSidenav, $location, recipeService) {
        $scope.recipeService = recipeService;
        $scope.$watch('$scope.recipeService.refresh', function (newValue) {
            if (newValue == true) {
                getRecipes();
            }
        });

        getRecipes();
        function getRecipes() {
            $scope.recipeService.list();
        }

        $scope.goHome = function () {
            $location.path('/');
        };

        //todo organize by category

        $scope.toggleSidenav = function (menuId) {
            $mdSidenav(menuId).toggle();
        };

        var self = this;
        // list of `state` value/display objects
        self.recipes = loadAll();
        self.selectedItem = null;
        self.searchText = null;
        self.querySearch = querySearch;
        self.simulateQuery = false;
        self.selectResult = selectResult;
        // ******************************
        // Internal methods
        // ******************************
        /**
         * Search for states... use $timeout to simulate
         * remote dataservice call.
         */
        function querySearch(query) {
            var results = query ? self.recipes.filter(createFilterFor(query)) : [],
                deferred;
            return results;
        }

        function selectResult(recipe) {
            if (recipe != undefined || recipe != null) {
                self.searchText = null;
                $location.path('/detail/' + recipe.recipeKey.id);
            }
        }

        /**
         * Build `states` list of key/value pairs
         */
        function loadAll() {
            return $scope.recipeService.recipes;
        }

        /**
         * Create filter function for a query string
         */
        function createFilterFor(query) {
            var lowercaseQuery = angular.lowercase(query);
            return function filterFn(recipe) {
                return (recipe.recipeName.indexOf(lowercaseQuery) >= 0);
            };
        }
    }]);

app.controller("AddController", ['$scope', '$location', 'GApi', '$rootScope', 'recipeService',
    function ($scope, $location, GApi, recipeService) {
        $scope.ingredients = [{id: '1'}];
        $scope.recipeService = recipeService;

        $scope.addNewIngredient = function () {
            var newItemNo = $scope.ingredients.length + 1;
            $scope.ingredients.push({'id': newItemNo});
        };

        $scope.steps = [{id: '1'}];

        $scope.addNewStep = function () {
            var newItemNo = $scope.steps.length + 1;
            $scope.steps.push({'id': newItemNo});
        };

        $scope.submitRecipe = function () {
            var recipeId;
            GApi.executeAuth('recipe', 'add',
                {
                    category: $scope.recipe.category,
                    recipeName: $scope.recipe.recipeName,
                    tags: $scope.recipe.tags
                }).then(function (resp) {
                    recipeId = resp.recipeKey.id;
                    //todo add ingredients/steps and go to detail in path
                    $scope.recipeService.refreshvalue = true;
                    $location.path('/detail/' + recipeId);
                }, function () {
                });
        };
    }]);

app.controller("DetailController", ['$scope', '$routeParams', 'GApi', 'recipeService',
    function ($scope, $routeParams, GApi, recipeService) {
        $scope.recipeService = recipeService;
        $scope.$watch('$scope.recipeService.refresh', function (newValue) {
            if (newValue == true) {
                getRecipes();
            }
        });

        getRecipes();
        function getRecipes() {
            $scope.recipeService.list();
        }

        GApi.executeAuth('recipe', 'getRecipe', {id: $routeParams.recipeId}).then(function (resp) {
            $scope.recipe = resp.result.properties;
        }, function () {
        });
    }]);

app.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider
            //todo add recipe list - copy current home.html to list.html
            //todo add profile
            //todo add settings
            .when('/', {
                templateUrl: '/include/home.html',
                controller: 'RecipeController'
            })
            .when('/add', {
                templateUrl: '/include/add.html',
                controller: 'AddController'
            })
            .when('/detail/:recipeId', {
                templateUrl: '/include/detail.html',
                controller: 'DetailController'
            })
    }]);

app.factory('recipeService', function (GApi) {
    var refreshvalue;
    var recipes = [];


    function list() {
        GApi.executeAuth('recipe', 'getAllRecipes').then(function (resp) {
            angular.copy(resp.items, recipes);
            refreshvalue = false;
        }, function () {
        });
    }

    return {
        recipes: recipes,
        list: list,
        refresh: refreshvalue
    }
});
